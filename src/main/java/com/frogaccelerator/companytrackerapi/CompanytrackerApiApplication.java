package com.frogaccelerator.companytrackerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CompanytrackerApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CompanytrackerApiApplication.class, args);
	}
}
