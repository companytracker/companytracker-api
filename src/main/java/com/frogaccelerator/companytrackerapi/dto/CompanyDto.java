package com.frogaccelerator.companytrackerapi.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.List;

@Getter
@ToString
@Builder
public class CompanyDto {
    private String symbol;
    private String name;
    private String logo;
    private String countryCode;
    private List<ReportDto> reports;
}
