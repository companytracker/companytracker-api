package com.frogaccelerator.companytrackerapi.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Builder
public class CountryDto {
    private String code;
    private String name;
}
