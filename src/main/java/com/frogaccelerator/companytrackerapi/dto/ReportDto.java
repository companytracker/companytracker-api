package com.frogaccelerator.companytrackerapi.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.time.LocalDate;

@Getter
@ToString
@Builder
public class ReportDto {
    private String periodType;
    private LocalDate end;
    private long revenue;
    private long ebitda;
    private long netIncome;
    private int employees;
    private int securityCount;
    private double securityPrice;
}
