package com.frogaccelerator.companytrackerapi.dto.security;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class CredentialsDto {
    private String username;
    private String password;
}
