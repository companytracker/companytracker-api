package com.frogaccelerator.companytrackerapi.dto.update;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Data
@ToString
public class CompanySaveDto {
    @NotBlank(message = "Symbol is mandatory")
    private String symbol;
    @NotBlank(message = "Name is mandatory")
    private String name;
    private String logo;
    @NotBlank(message = "CountryCode is mandatory")
    private String countryCode;
}
