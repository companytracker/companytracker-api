package com.frogaccelerator.companytrackerapi.dto.update;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import java.time.LocalDate;

@Data
@ToString
public class ReportSaveDto {
    @NotBlank(message = "CompanySymbol is mandatory")
    private String companySymbol;
    @NotBlank(message = "ReportType is mandatory")
    private String reportType;
    @Past(message = "Report end time is mandatory and must be in the past")
    private LocalDate end;
    private long revenue;
    private long ebitda;
    private long netIncome;
    private int employees;
    private int securityCount;
    private double securityPrice;
}