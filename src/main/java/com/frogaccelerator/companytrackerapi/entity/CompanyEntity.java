package com.frogaccelerator.companytrackerapi.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "company")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CompanyEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(unique = true, length = 20)
    private String symbol;
    private String name;
    private String logo;
    @ManyToOne
    private CountryEntity country;
    @OneToMany(mappedBy = "company")
    List<ReportEntity> reports;
}
