package com.frogaccelerator.companytrackerapi.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "country")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class CountryEntity {
    @Id
    @Column(length = 3)
    private String code;
    @Column(unique = true, length = 100)
    private String name;
}
