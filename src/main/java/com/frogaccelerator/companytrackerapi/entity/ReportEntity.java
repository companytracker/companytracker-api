package com.frogaccelerator.companytrackerapi.entity;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "report")
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ReportEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    private CompanyEntity company;
    private PeriodType periodType;
    private LocalDate periodEnd;
    private long revenue;
    private long ebitda;
    private long netIncome;
    private int employees;
    private int securityCount;
    private double securityPrice;

    public enum PeriodType {
        ANNUAL, QUARTERLY
    }
}
