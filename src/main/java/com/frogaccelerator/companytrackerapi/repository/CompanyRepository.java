package com.frogaccelerator.companytrackerapi.repository;

import com.frogaccelerator.companytrackerapi.entity.CompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CompanyRepository extends JpaRepository<CompanyEntity, Integer> {

    Optional<CompanyEntity> findBySymbol(String symbol);
}
