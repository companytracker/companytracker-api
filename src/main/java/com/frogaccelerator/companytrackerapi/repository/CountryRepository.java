package com.frogaccelerator.companytrackerapi.repository;

import com.frogaccelerator.companytrackerapi.entity.CountryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<CountryEntity, String> {
}
