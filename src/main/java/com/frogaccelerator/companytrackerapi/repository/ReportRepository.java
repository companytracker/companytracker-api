package com.frogaccelerator.companytrackerapi.repository;

import com.frogaccelerator.companytrackerapi.entity.CompanyEntity;
import com.frogaccelerator.companytrackerapi.entity.ReportEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;

public interface ReportRepository extends JpaRepository<ReportEntity, String> {

    Optional<ReportEntity> findByCompanyAndPeriodTypeAndPeriodEnd(CompanyEntity company, ReportEntity.PeriodType periodType, LocalDate periodEnd);
}
