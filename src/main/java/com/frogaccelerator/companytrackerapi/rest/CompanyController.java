package com.frogaccelerator.companytrackerapi.rest;

import com.frogaccelerator.companytrackerapi.dto.CompanyDto;
import com.frogaccelerator.companytrackerapi.dto.CountryDto;
import com.frogaccelerator.companytrackerapi.dto.update.CompanySaveDto;
import com.frogaccelerator.companytrackerapi.dto.update.ReportSaveDto;
import com.frogaccelerator.companytrackerapi.service.CompanyService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class CompanyController {

    private final CompanyService companyService;

    @GetMapping("public/countries")
    public List<CountryDto> getCountries() {
        return companyService.getCountries();
    }

    @GetMapping("public/companies")
    public List<CompanyDto> getCompanies() {
        return companyService.getCompanies();
    }

    @PostMapping("management/company")
    public void saveCompany(@Valid @RequestBody CompanySaveDto companyDto) {
        companyService.saveCompany(companyDto);
    }

    @PostMapping("management/report")
    public void saveReport(@Valid @RequestBody ReportSaveDto reportDto) {
        companyService.saveReport(reportDto);
    }
}
