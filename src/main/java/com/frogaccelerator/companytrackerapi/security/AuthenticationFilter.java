package com.frogaccelerator.companytrackerapi.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.frogaccelerator.companytrackerapi.dto.security.CredentialsDto;
import com.frogaccelerator.companytrackerapi.security.token.TokenDetails;
import com.frogaccelerator.companytrackerapi.security.token.TokenUtil;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.ZoneId;
import java.util.ArrayList;

import static com.frogaccelerator.companytrackerapi.security.SecurityConstants.*;

@AllArgsConstructor
public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private final TokenUtil tokenUtil;

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        try {
            CredentialsDto credentials = new ObjectMapper().readValue(request.getInputStream(), CredentialsDto.class);

            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            credentials.getUsername(),
                            credentials.getPassword(),
                            new ArrayList<>())
            );
        } catch (IOException e) {
            System.out.println("Authentication failed!");
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        User user = (User) authResult.getPrincipal();

        TokenDetails[] tokens = tokenUtil.generateTokens(user.getUsername());
        TokenDetails headerToken = tokens[0];
        TokenDetails cookieToken = tokens[1];

        response.addHeader(HEADER_AUTHORIZATION, HEADER_AUTHORIZATION_TOKEN_PREFIX + headerToken.getSignedToken());
        String expiresAt = headerToken.getExpirationTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().toString();
        response.setHeader(HEADER_EXPIRES, expiresAt);

        Cookie authCookie = new Cookie(COOKIE_AUTHORIZATION, cookieToken.getSignedToken());
        authCookie.setPath("/");
        authCookie.setHttpOnly(true);
        authCookie.setMaxAge(cookieToken.getMaxAge());
        response.addCookie(authCookie);
    }
}
