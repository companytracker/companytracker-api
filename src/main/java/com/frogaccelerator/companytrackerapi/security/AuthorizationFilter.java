package com.frogaccelerator.companytrackerapi.security;

import com.frogaccelerator.companytrackerapi.security.token.TokenDetails;
import com.frogaccelerator.companytrackerapi.security.token.TokenUtil;
import com.frogaccelerator.companytrackerapi.security.token.TokenValidationException;
import com.frogaccelerator.companytrackerapi.service.UserService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Stream;

import static com.frogaccelerator.companytrackerapi.security.SecurityConstants.*;

@AllArgsConstructor
public class AuthorizationFilter extends OncePerRequestFilter {

    private final UserService userService;
    private final TokenUtil tokenUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        TokenDetails headerToken = null;
        TokenDetails cookieToken = null;
        try {
            headerToken = getHeaderToken(request);
            cookieToken = getCookieToken(request);
            if (headerToken != null && cookieToken != null) {
                tokenUtil.validateTokens(headerToken, cookieToken);
                UserDetails userDetails = this.userService.loadUserByUsername(headerToken.getUsername());
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                        new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        } catch(MalformedJwtException e) {
            System.out.println("Incorrect token!");
        } catch (IllegalArgumentException e) {
            System.out.println("Unable to parse Token!");
        } catch (SignatureException e) {
            System.out.println("Token signature mismatch!");
        } catch (ExpiredJwtException e) {
            System.out.println("Token has expired!");
        } catch (UsernameNotFoundException e) {
            System.out.println("User not found!");
        } catch (TokenValidationException e) {
            System.out.println("Token validation failed!");
            System.out.println(e.getMessage());
            System.out.println(headerToken);
            System.out.println(cookieToken);
        }
        filterChain.doFilter(request, response);
    }

    private TokenDetails getHeaderToken(HttpServletRequest request) {
        String headerToken = request.getHeader(HEADER_AUTHORIZATION);
        if (headerToken != null && headerToken.startsWith(HEADER_AUTHORIZATION_TOKEN_PREFIX)) {
            return tokenUtil.parseToken(headerToken.substring(7));
        } else {
            return null;
        }
    }

    private TokenDetails getCookieToken(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies() != null ? request.getCookies() : new Cookie[0];
        Cookie authCookie = Stream.of(cookies).filter(c -> c.getName().equals(COOKIE_AUTHORIZATION)).findFirst().orElse(null);
        return authCookie != null ? tokenUtil.parseToken(authCookie.getValue()) : null;
    }
}
