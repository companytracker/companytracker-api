package com.frogaccelerator.companytrackerapi.security;

public class SecurityConstants {
    public static final String TOKEN_SYNC_ID = "syncId";
    public static final String TOKEN_TYPE = "type";

    public static final String HEADER_EXPIRES = "Expires";
    public static final String HEADER_AUTHORIZATION = "Authorization";
    public static final String HEADER_AUTHORIZATION_TOKEN_PREFIX = "Bearer ";

    public static final String COOKIE_AUTHORIZATION = "Authorization";
}
