package com.frogaccelerator.companytrackerapi.security.token;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.Date;

@Getter
@Builder
@ToString
public class TokenDetails {
    private final String username;
    private final String syncId;
    private final Type type;
    private final Date issuedAt;
    private final Date expirationTime;
    private final int maxAge;
    private final String signedToken;

    public enum Type {
        HEADER, COOKIE
    }
}
