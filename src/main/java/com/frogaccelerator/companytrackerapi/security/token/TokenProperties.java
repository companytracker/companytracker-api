package com.frogaccelerator.companytrackerapi.security.token;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "jwt")
public class TokenProperties {
    private String secret;
    private Long expiry;
}
