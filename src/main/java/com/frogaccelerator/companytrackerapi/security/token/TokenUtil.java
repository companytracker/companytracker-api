package com.frogaccelerator.companytrackerapi.security.token;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

import static com.frogaccelerator.companytrackerapi.security.SecurityConstants.TOKEN_SYNC_ID;
import static com.frogaccelerator.companytrackerapi.security.SecurityConstants.TOKEN_TYPE;

@Service
@AllArgsConstructor
public class TokenUtil {

    private final TokenProperties tokenProperties;

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(tokenProperties.getSecret())
                .parseClaimsJws(token)
                .getBody();
    }

    private TokenDetails generateToken(String username, String syncId, TokenDetails.Type type, Date issuedAt, Date expiration) {
        String token = Jwts.builder()
                .setSubject(username)
                .claim(TOKEN_SYNC_ID, syncId)
                .claim(TOKEN_TYPE, type)
                .setIssuedAt(issuedAt)
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS512, tokenProperties.getSecret())
                .compact();

        return TokenDetails.builder()
                .username(username)
                .syncId(syncId)
                .type(type)
                .issuedAt(issuedAt)
                .expirationTime(expiration)
                .maxAge(tokenProperties.getExpiry().intValue())
                .signedToken(token)
                .build();
    }

    public TokenDetails[] generateTokens(String username) {
        String syncString = UUID.randomUUID().toString();
        Date issuedAt = new Date();
        Date expiration = new Date(System.currentTimeMillis() + tokenProperties.getExpiry() * 1000);

        TokenDetails headerToken = generateToken(username, syncString, TokenDetails.Type.HEADER, issuedAt, expiration);
        TokenDetails cookieToken = generateToken(username, syncString, TokenDetails.Type.COOKIE, issuedAt, expiration);

        return new TokenDetails[]{headerToken, cookieToken};
    }

    public TokenDetails parseToken(String token) {
        Claims claims = getAllClaimsFromToken(token);

        return TokenDetails.builder()
                .username(claims.getSubject())
                .syncId(claims.get(TOKEN_SYNC_ID, String.class))
                .type(TokenDetails.Type.valueOf(claims.get(TOKEN_TYPE, String.class)))
                .issuedAt(claims.getIssuedAt())
                .expirationTime(claims.getExpiration())
                .maxAge(tokenProperties.getExpiry().intValue())
                .signedToken(token)
                .build();
    }

    public void validateTokens(TokenDetails headerToken, TokenDetails cookieToken) {
        if (!headerToken.getUsername().equals(cookieToken.getUsername())) {
            throw new TokenValidationException("Username mismatch in tokens!");
        } else if (!headerToken.getSyncId().equals(cookieToken.getSyncId())) {
            throw new TokenValidationException("Sync IDs do not match!");
        } else if (headerToken.getType() != TokenDetails.Type.HEADER) {
            throw new TokenValidationException("Header token type mismatch!");
        } else if (cookieToken.getType() != TokenDetails.Type.COOKIE) {
            throw new TokenValidationException("Cookie token type mismatch!");
        }
    }
}
