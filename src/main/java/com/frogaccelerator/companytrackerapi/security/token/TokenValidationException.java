package com.frogaccelerator.companytrackerapi.security.token;

public class TokenValidationException extends RuntimeException {
    public TokenValidationException(String message) {
        super(message);
    }
}
