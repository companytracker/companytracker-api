package com.frogaccelerator.companytrackerapi.service;

import com.frogaccelerator.companytrackerapi.dto.CompanyDto;
import com.frogaccelerator.companytrackerapi.dto.CountryDto;
import com.frogaccelerator.companytrackerapi.dto.ReportDto;
import com.frogaccelerator.companytrackerapi.dto.update.CompanySaveDto;
import com.frogaccelerator.companytrackerapi.dto.update.ReportSaveDto;
import com.frogaccelerator.companytrackerapi.entity.CompanyEntity;
import com.frogaccelerator.companytrackerapi.entity.CountryEntity;
import com.frogaccelerator.companytrackerapi.entity.ReportEntity;
import com.frogaccelerator.companytrackerapi.repository.CompanyRepository;
import com.frogaccelerator.companytrackerapi.repository.CountryRepository;
import com.frogaccelerator.companytrackerapi.repository.ReportRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class CompanyService {

    private final CountryRepository countryRepository;
    private final CompanyRepository companyRepository;
    private final ReportRepository reportRepository;

    public List<CountryDto> getCountries() {
        return countryRepository.findAll().stream()
                .map(ce -> CountryDto.builder()
                        .code(ce.getCode())
                        .name(ce.getName())
                        .build())
                .collect(Collectors.toList());
    }

    public List<CompanyDto> getCompanies() {
        return companyRepository.findAll().stream()
                .map(ce -> CompanyDto.builder()
                        .symbol(ce.getSymbol())
                        .name(ce.getName())
                        .logo(ce.getLogo())
                        .countryCode(ce.getCountry().getCode())
                        .reports(ce.getReports().stream()
                                .map(re -> ReportDto.builder()
                                        .periodType(re.getPeriodType().name())
                                        .end(re.getPeriodEnd())
                                        .revenue(re.getRevenue())
                                        .ebitda(re.getEbitda())
                                        .netIncome(re.getNetIncome())
                                        .employees(re.getEmployees())
                                        .securityCount(re.getSecurityCount())
                                        .securityPrice(re.getSecurityPrice())
                                        .build())
                                .collect(Collectors.toList()))
                        .build())
                .collect(Collectors.toList());
    }

    public void saveCompany(CompanySaveDto companyDto) {
        Optional<CountryEntity> countryEntity = countryRepository.findById(companyDto.getCountryCode());
        Assert.isTrue(countryEntity.isPresent(), "Unknown country");

        CompanyEntity companyEntity = companyRepository.findBySymbol(companyDto.getSymbol()).orElse(new CompanyEntity());
        companyEntity.setSymbol(companyDto.getSymbol());
        companyEntity.setName(companyDto.getName());
        companyEntity.setLogo(companyDto.getLogo());
        companyEntity.setCountry(countryEntity.get());
        companyRepository.save(companyEntity);
    }

    public void saveReport(ReportSaveDto reportDto) {
        Optional<CompanyEntity> company = companyRepository.findBySymbol(reportDto.getCompanySymbol());
        ReportEntity.PeriodType periodType = ReportEntity.PeriodType.valueOf(reportDto.getReportType());
        LocalDate periodEnd = reportDto.getEnd();

        Assert.isTrue(company.isPresent(), "Unknown company");
        Assert.isTrue(periodEnd != null, "Report end time not specified");

        ReportEntity reportEntity = reportRepository.findByCompanyAndPeriodTypeAndPeriodEnd(company.get(), periodType, periodEnd).orElse(new ReportEntity());
        reportEntity.setCompany(company.get());
        reportEntity.setPeriodType(periodType);
        reportEntity.setPeriodEnd(periodEnd);
        reportEntity.setRevenue(reportDto.getRevenue());
        reportEntity.setEbitda(reportDto.getEbitda());
        reportEntity.setNetIncome(reportDto.getNetIncome());
        reportEntity.setEmployees(reportDto.getEmployees());
        reportEntity.setSecurityCount(reportDto.getSecurityCount());
        reportEntity.setSecurityPrice(reportDto.getSecurityPrice());
        reportRepository.save(reportEntity);
    }
}
