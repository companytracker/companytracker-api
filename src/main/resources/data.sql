INSERT INTO "user" ("username", "password") VALUES ('admin', '$2a$10$iz1QiuSVUMFcXdx5Xs2mr.OdTZBuutK0GrKqWS8x2OhaDz8jKV5ei');

insert into "country" ("code", "name") values ('EST', 'Estonia');
insert into "country" ("code", "name") values ('LAT', 'Latvia');
insert into "country" ("code", "name") values ('LIT', 'Lithuania');
