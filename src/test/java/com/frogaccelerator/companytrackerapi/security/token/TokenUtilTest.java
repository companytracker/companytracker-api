package com.frogaccelerator.companytrackerapi.security.token;

import io.jsonwebtoken.ExpiredJwtException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

public class TokenUtilTest {

    private static final long expiry = 1L;

    private static TokenUtil tokenUtil1 = null;
    private static TokenUtil tokenUtil2 = null;

    @BeforeAll
    static void setUp() {
        TokenProperties tokenProperties1 = new TokenProperties();
        tokenProperties1.setExpiry(expiry);
        tokenProperties1.setSecret("saladus1");
        tokenUtil1 = new TokenUtil(tokenProperties1);

        TokenProperties tokenProperties2 = new TokenProperties();
        tokenProperties1.setExpiry(expiry);
        tokenProperties1.setSecret("saladus2");
        tokenUtil2 = new TokenUtil(tokenProperties2);
    }

    @Test
    public void testGenerateTokens() {
        String testuser = "testuser";

        TokenDetails[] tokens = tokenUtil1.generateTokens(testuser);

        Assertions.assertNotNull(tokens);

        TokenDetails headerTokenDetails = tokens[0];
        TokenDetails cookieTokenDetails = tokens[1];

        Assertions.assertNotNull(headerTokenDetails);
        Assertions.assertNotNull(cookieTokenDetails);

        Assertions.assertEquals(headerTokenDetails.getUsername(), cookieTokenDetails.getUsername());
        Assertions.assertEquals(headerTokenDetails.getSyncId(), cookieTokenDetails.getSyncId());
        Assertions.assertEquals(headerTokenDetails.getIssuedAt(), cookieTokenDetails.getIssuedAt());
        Assertions.assertEquals(headerTokenDetails.getExpirationTime(), cookieTokenDetails.getExpirationTime());
        Assertions.assertEquals(headerTokenDetails.getMaxAge(), cookieTokenDetails.getMaxAge());

        Assertions.assertEquals(headerTokenDetails.getUsername(), testuser);
        Assertions.assertEquals(headerTokenDetails.getType(), TokenDetails.Type.HEADER);

        Assertions.assertEquals(cookieTokenDetails.getType(), TokenDetails.Type.COOKIE);
    }

    @Test
    public void testParseTokens() {
        String testuser = "testuser";

        TokenDetails[] tokens = tokenUtil1.generateTokens(testuser);

        TokenDetails headerTokenDetails = tokenUtil1.parseToken(tokens[0].getSignedToken());
        TokenDetails cookieTokenDetails = tokenUtil1.parseToken(tokens[1].getSignedToken());

        Assertions.assertNotNull(headerTokenDetails);
        Assertions.assertNotNull(cookieTokenDetails);

        Assertions.assertEquals(headerTokenDetails.getUsername(), cookieTokenDetails.getUsername());
        Assertions.assertEquals(headerTokenDetails.getSyncId(), cookieTokenDetails.getSyncId());
        Assertions.assertEquals(headerTokenDetails.getIssuedAt(), cookieTokenDetails.getIssuedAt());
        Assertions.assertEquals(headerTokenDetails.getExpirationTime(), cookieTokenDetails.getExpirationTime());
        Assertions.assertEquals(headerTokenDetails.getMaxAge(), cookieTokenDetails.getMaxAge());

        Assertions.assertEquals(headerTokenDetails.getUsername(), testuser);
        Assertions.assertEquals(headerTokenDetails.getType(), TokenDetails.Type.HEADER);

        Assertions.assertEquals(cookieTokenDetails.getType(), TokenDetails.Type.COOKIE);
    }

    @Test
    public void testParseTokensWithIncorrectSecret() {
        String testuser = "testuser";

        TokenDetails[] tokens = tokenUtil1.generateTokens(testuser);

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            tokenUtil2.parseToken(tokens[0].getSignedToken());
        });

        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            tokenUtil2.parseToken(tokens[1].getSignedToken());
        });
    }

    @Test
    public void testParseExpiredTokens() throws InterruptedException {
        String testuser = "testuser";

        TokenDetails[] tokens = tokenUtil1.generateTokens(testuser);

        TimeUnit.SECONDS.sleep(expiry + 1);

        Assertions.assertThrows(ExpiredJwtException.class, () -> {
            tokenUtil1.parseToken(tokens[0].getSignedToken());
        });
    }

    @Test
    public void testValidateTokens() {
        String testuser = "testuser";

        TokenDetails[] tokens = tokenUtil1.generateTokens(testuser);

        TokenDetails headerTokenDetails = tokenUtil1.parseToken(tokens[0].getSignedToken());
        TokenDetails cookieTokenDetails = tokenUtil1.parseToken(tokens[1].getSignedToken());

        tokenUtil1.validateTokens(headerTokenDetails, cookieTokenDetails);
    }

    @Test
    public void testValidateNonPairedTokens() {
        String testuser = "testuser";

        TokenDetails[] tokens1 = tokenUtil1.generateTokens(testuser);
        TokenDetails[] tokens2 = tokenUtil1.generateTokens(testuser);

        TokenDetails headerTokenDetails = tokens1[0];
        TokenDetails cookieTokenDetails = tokens2[1];

        Assertions.assertThrows(TokenValidationException.class, () -> {
            tokenUtil1.validateTokens(headerTokenDetails, cookieTokenDetails);
        });
    }

}
